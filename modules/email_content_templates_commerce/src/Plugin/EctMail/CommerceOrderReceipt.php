<?php

namespace Drupal\email_content_templates_commerce\Plugin\EctMail;

use Drupal\email_content_templates\Annotation\EctMailPlugin;
use Drupal\email_content_templates\Entity\EmailContentTemplateInterface;

/**
 * Plugin implementation of the commerce order receipt.
 *
 * @EctMailPlugin(
 *   id = "commerce-order_receipt",
 *   label = @Translation("Commerce order receipt"),
 *   category = "Commerce",
 * )
 */
class CommerceOrderReceipt extends EctCommerceMailPluginBase {

  /**
   * {@inheritdoc}
   */
  public function paramTokenMap() {
    return [
      'order' => 'commerce_order',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMailVariables() {
    return [
      'body' => $this->t('The original commerce order receipt template.'),
    ] + parent::getMailVariables();
  }

  /**
   * {@inheritdoc}
   */
  public function preRenderAlterMail(EmailContentTemplateInterface $template, array &$message): void {
    $order = $message['params']['order'];

    // Render order entity as body.
    // $build = \Drupal::entityTypeManager()->getViewBuilder($order->getEntityTypeId())->view($order, 'email');
    // @todo consider providing a setting to choose.
    // wether to render entity or theme or if even necessary.
    $body = [
      '#theme' => 'commerce_order_receipt',
      '#order_entity' => $order,
      '#totals' => $this->orderTotalSummary->buildTotals($order),
    ];
    if ($billing_profile = $order->getBillingProfile()) {
      $profile_view_builder = $this->entityTypeManager->getViewBuilder('profile');
      $body['#billing_information'] = $profile_view_builder->view($billing_profile);
    }

    $message['params']['body'] = $body;
    parent::preRenderAlterMail($template, $message);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultText() {
    return '<h1>x<a href="{{ url(\'<front>\') }}">{{ store }}</a></h1>
<table class="commerce-order-body"><tr><td>
<h2>Order Confirmation</h2>
<p class="commerce-order-number">Order #{{ order_number }} details</p>
{{ body }}
<p>Thank you for your order!</p>
</td></tr></table>';
  }

}
