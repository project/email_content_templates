<?php

namespace Drupal\email_content_templates_commerce\Plugin\EctMail;

use Drupal\email_content_templates\Annotation\EctMailPlugin;
use Drupal\email_content_templates\Entity\EmailContentTemplateInterface;

/**
 * Plugin implementation of the commerce invoice confirmation.
 *
 * @EctMailPlugin(
 *   id = "commerce-invoice_confirmation",
 *   label = @Translation("Commerce invoice confirmation"),
 *   category = "Commerce",
 * )
 */
class CommerceInvoiceConfirmation extends EctCommerceMailPluginBase {

  /**
   * {@inheritdoc}
   */
  public function paramTokenMap() {
    return [
      'order' => 'commerce_order',
      'invoice' => 'commerce_invoice',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preRenderAlterMail(EmailContentTemplateInterface $template, array &$message): void {
    /** @var \Drupal\commerce_invoice\Entity\InvoiceInterface $invoice */
    $invoice = $message['params']['invoice'];
    $message['params']['order'] = current($invoice->getOrders());
    parent::preRenderAlterMail($template, $message);
  }

}
