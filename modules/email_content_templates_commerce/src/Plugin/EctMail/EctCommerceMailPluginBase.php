<?php

namespace Drupal\email_content_templates_commerce\Plugin\EctMail;

use Drupal\commerce\ConditionGroup;
use Drupal\commerce\Plugin\Commerce\Condition\ParentEntityAwareInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\email_content_templates\Plugin\EctMail\EctMailPluginBase;
use Drupal\email_content_templates\Entity\EmailContentTemplateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Abstract base class for commerce mail plugins.
 */
abstract class EctCommerceMailPluginBase extends EctMailPluginBase implements EctCommerceOrderMailPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The order total summary builder.
   *
   * @var \Drupal\commerce_order\OrderTotalSummaryInterface
   */
  protected $orderTotalSummary;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->conditionsManager = $container->get('plugin.manager.commerce_condition');
    $instance->orderTotalSummary = $container->get('commerce_order.order_total_summary');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConditionsForm(EmailContentTemplateInterface $template, FormStateInterface $form_state) {
    $form['conditions'] = [
      '#type' => 'commerce_conditions',
      '#title' => $this->t('Conditions'),
      '#parent_entity_type' => 'commerce_payment_gateway',
      '#entity_types' => ['commerce_order'],
      '#default_value' => $template->getConditions()['conditions'] ?? [],
      '#parents' => ['conditions'],
    ];
    $form['conditionOperator'] = [
      '#type' => 'radios',
      '#title' => $this->t('Condition operator'),
      '#title_display' => 'invisible',
      '#options' => [
        'AND' => $this->t('All conditions must pass'),
        'OR' => $this->t('Only one condition must pass'),
      ],
      '#default_value' => $template->getConditions()['conditionOperator'] ?? 'AND',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getMailVariables() {
    return [
      'order_number' => $this->t('The order number'),
      'store' => $this->t('Name of the store'),
    ] + parent::getMailVariables();
  }

  /**
   * {@inheritdoc}
   */
  public function preRenderAlterMail(EmailContentTemplateInterface $template, array &$message): void {
    $message['params']['order_number'] = $message['params']['order']->getOrderNumber();
    $message['params']['store'] = $message['params']['order']->getStore()->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function applies(EmailContentTemplateInterface $template, array $message): bool {
    $plugin = $template->getEmailPlugin();
    if (empty($plugin)) {
      return FALSE;
    }

    $condition_settings = $template->getConditions();
    if (!isset($condition_settings['conditions']) || count($condition_settings['conditions']) === 0) {
      // Templates without conditions always apply.
      return TRUE;
    }

    $conditions = $this->loadConditions($template);
    $conditions = array_filter($conditions, function ($condition) {
      /** @var \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface $condition */
      return $condition->getEntityTypeId() == 'commerce_order';
    });
    $condition_group = new ConditionGroup($conditions, $condition_settings['conditionOperator']);

    return $condition_group->evaluate($message['params']['order']);
  }

  /**
   * Gets the conditions.
   *
   * @return \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface[]
   *   The conditions.
   */
  protected function loadConditions(EmailContentTemplateInterface $template): array {
    $conditions = [];
    foreach ($template->getConditions()['conditions'] as $condition_settings) {
      $condition = $this->conditionsManager->createInstance($condition_settings['plugin'], $condition_settings['configuration']);
      if ($condition instanceof ParentEntityAwareInterface) {
        $condition->setParentEntity($this);
      }
      $conditions[] = $condition;
    }
    return $conditions;
  }

}
