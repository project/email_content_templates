<?php

namespace Drupal\email_content_templates_vbo\Plugin\Action;

use Drupal;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\Attribute\Action;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\email_content_templates\EctMailPluginManager;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

#[Action(
    id: 'email_content_templates_vbo_send_email_action',
    label: new TranslatableMarkup('Send email template'),
    type: 'user'
)]
class SendEmailAction extends ViewsBulkOperationsActionBase implements ContainerFactoryPluginInterface
{

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $templates = $this->entityTypeManager
            ->getStorage('email_content_template')->loadByProperties([]);

        $options = [];
        foreach ($templates as $template) {
            $options[$template->id()] = $template->label();
        }

        $form['template'] = [
            '#type' => 'select',
            '#title' => $this->t('Template'),
            '#options' => $options,
            '#required' => TRUE,
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE)
    {
        return $return_as_object ? AccessResult::allowed() : TRUE;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($account = NULL)
    {
        $template = $this->entityTypeManager->getStorage('email_content_template')
            ->load($this->configuration['template']);
        $email_plugin = $this->mailPluginManager->createInstance($template->getEmailPluginId());
        $this->mailManager->mail($email_plugin->getMailModule(), $email_plugin->getMailKey(), $account->getEmail(), $account->getPreferredLangcode(), ['account' => $account]);

    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('entity_type.manager'),
            $container->get('plugin.manager.ect_mail'),
            $container->get('plugin.manager.mail'),
        );
    }

    public function __construct($configuration, $plugin_id, $plugin_definition, protected EntityTypeManagerInterface $entityTypeManager, protected EctMailPluginManager $mailPluginManager, protected MailManagerInterface $mailManager)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
    }

}
