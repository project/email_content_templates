<?php

/**
 * @file
 * Describes hooks and plugins provided by the module.
 */

use Drupal\symfony_mailer\EmailInterface;

/**
 * Hook to allow modules to build a mail message info for template.
 *
 * Required are 'module' and 'key' to determine mail template.
 *
 * @param \Drupal\symfony_mailer\EmailInterface $email
 *   The symfony mail.
 *
 * @return array
 *   The mail message array.
 */
function hook_ect_build_EMAIL_BUILDER_ID_message(EmailInterface $email) {
}
