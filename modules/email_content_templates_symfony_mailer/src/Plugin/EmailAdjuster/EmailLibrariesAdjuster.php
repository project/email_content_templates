<?php

namespace Drupal\email_content_templates_symfony_mailer\Plugin\EmailAdjuster;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\symfony_mailer\Annotation\EmailAdjuster;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\EmailAdjusterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Automatically adds additional css libraries for emails.
 *
 * CSS in emails is pretty restricted. There are many things that do not work as
 * you expect on a website like media queries, so it is recommended to prepare a
 * CSS file for explicit email use only.
 *
 * Add a new library to your THEME.libraries.yml using the 'email' key
 * and add your css file(s):
 * email:
 *   css:
 *     component:
 *       css/mail.css: {}
 *
 * If needed, you can provide extra CSS for specific emails
 * which need special styling because of their special templates:
 *
 * email_EMAIL_SUBTYPE:
 *   css:
 *     component:
 *       css/additional_mail.css: {}
 *
 * E.g. email_order_receipt
 *
 * Libraries of all parent base themes are also added.
 *
 * @EmailAdjuster(
 *   id = "ect_email_libraries_adjuster",
 *   label = @Translation("Mail CSS Libraries"),
 *   description = @Translation("Set html mail styles."),
 *   automatic = TRUE,
 *   weight = 100,
 * )
 */
class EmailLibrariesAdjuster extends EmailAdjusterBase implements ContainerFactoryPluginInterface {

  /**
   * The theme extension list.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected ThemeExtensionList $themeList;

  /**
   * Drupal\Core\Asset\LibraryDiscoveryInterface definition.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected LibraryDiscoveryInterface $libraryDiscovery;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );

    $instance->themeList = $container->get('extension.list.theme');
    $instance->libraryDiscovery = $container->get('library.discovery');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
    $list = $this->themeList->getList();

    $themes = [];
    $loop = TRUE;
    $theme = $email->getTheme();
    do {
      if (isset($list[$theme])) {
        $themes[] = $theme;

        if (isset($list[$theme]->base_theme)) {
          $theme = $list[$theme]->base_theme;
        }
        else {
          $loop = FALSE;
        }
      }
      else {
        $loop = FALSE;
      }
    } while($loop);

    $themes = array_reverse($themes);
    foreach ($themes as $theme) {
      $theme_libraries = $this->libraryDiscovery->getLibrariesByExtension($theme);

      foreach (['email', 'email_' . $email->getSubType()] as $lib) {
        if (isset($theme_libraries[$lib])) {
          $email->addLibrary("$theme/$lib");
        }
      }
    }
  }

}
