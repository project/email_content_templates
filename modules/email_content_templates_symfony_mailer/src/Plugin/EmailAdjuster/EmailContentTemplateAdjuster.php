<?php

namespace Drupal\email_content_templates_symfony_mailer\Plugin\EmailAdjuster;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\symfony_mailer\Annotation\EmailAdjuster;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\EmailAdjusterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Body Email Adjuster.
 *
 * @EmailAdjuster(
 *   id = "email_content_template_adjuster",
 *   label = @Translation("Email Content Template"),
 *   description = @Translation("Insert an email content template."),
 *   weight = 800,
 * )
 */
class EmailContentTemplateAdjuster extends EmailAdjusterBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The email content templates service.
   *
   * @var \Drupal\email_content_templates\EctServiceInterface
   */
  protected $EctService;

  /**
   * The generated message.
   *
   * @var array
   */
  private $generatedMessage = NULL;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );

    $instance->moduleHandler = $container->get('module_handler');
    $instance->EctService = $container->get('email_content_templates.service');
    return $instance;
  }

  public function preRender(EmailInterface $email) {
    $mail_message = $this->generatedMessage;
    if ($mail_message === NULL) {
      return ;
    }
    if ($this->configuration['replace_subject']) {
      $email->setSubject($mail_message['subject'], TRUE);
    }

    if ($this->configuration['replace_body']) {
      $email->setBody($mail_message['body']);
    }

  }
  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
    if (!$this->configuration['replace_subject'] && !$this->configuration['replace_body']) {
      // Nothing to replace.
      return;
    }

    if (!$mail_message = $email->getParam('legacy_message')) {
      // The mail message used to tell us the mail id & key, which represents
      // the mail plugin id.
      // This email is not built by the legacy email builder, thus we are
      // missing the mail message array. We need a module to give some info.
      // @todo find a better way to determine the go to plugin id?
      /** @var \Drupal\symfony_mailer\Processor\EmailBuilderInterface $emailBuilderManager */
      $mail_message = $this->moduleHandler->invokeAll('ect_build_' . $email->getType() . '_message', [$email]);
    }
    if (empty($mail_message)) {
      return;
    }

    $templates = $this->EctService->findTemplatesForMail($mail_message);
    if (empty($templates)) {
      // No matching template found.
      return;
    }

    /** @var9 \Drupal\email_content_templates\Entity\EmailContentTemplateInterface $template */
    $template = current($templates);
    $this->EctService->prepareMessage($mail_message, $template);
    $email->setVariable('#email_content_template', $template);

    $this->generatedMessage = $mail_message;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['replace_subject'] = [
      '#title' => $this->t('Replace subject from content templates'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['replace_subject'] ?? TRUE,
    ];

    $form['replace_body'] = [
      '#title' => $this->t('Replace body from content templates'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['replace_body'] ?? TRUE,
    ];

    return $form;
  }

}
