<?php

/**
 * @file
 * Describes hooks and plugins provided by the module.
 */

use Drupal\symfony_mailer\EmailInterface;

/**
 * Hook to allow modules to alter the results array for selecting a template.
 *
 * @param \Drupal\email_content_templates\Entity\EmailContentTemplateInterface[] $results
 *   The templates array.
 * @param array $mail
 *   The mail message array.
 */
function hook_email_content_templates_alter(array &$results, array $mail) {
  foreach ($results as $key => $template) {
    if ($template->get('condition')->value === FALSE) {
      // Remove a template from results list if some condition is not met.
      unset($results[$key]);
    }
  }

  // Add a template default template to the results list.
  $results[] = 'default_template';
}

/**
 * Hook to allow modules to alter available variables in template.
 *
 * @param $variables
 *   The variables.
 */
function hook_mail_plugin_MAIL_PLUGIN_ID_alter(&$variables) {
  // Add some information.
  $variables['var'] = TRUE;
}

/**
 * Hook to provide variables information.
 *
 * This hook allows modules to provide additional information about variables
 * available in the mail template being used.
 * This will be displayed below the template body field, when editing an email
 * content template entity.
 *
 * If you add variables in hook_mail_plugin_MAIL_PLUGIN_ID_alter() you might
 * also want to add them here to make them visible.
 *
 * @param array $original_variables
 *   The original provided variables by the plugin.
 */
function hook_mail_plugin_MAIL_PLUGIN_ID_variables_info(array $original_variables) {
  return [
    'custom_var' => t('Custom variable'),
  ];
}
