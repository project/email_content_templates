<?php

namespace Drupal\Tests\email_content_templates\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Base kernel test.
 *
 * @group email_content_templates
 */
class CommerceRecruitingKernelTestBase extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->enableModules(['email_content_templates']);

    $user = $this->createUser();
    $this->user = $this->reloadEntity($user);
    $this->container->get('current_user')->setAccount($user);

    // Reset entity type manager otherwise commerce_recruiting not found.
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
  }

  /**
   * @return void
   */
  protected function createEmailContentTemplate() {
    // @todo TBD.
  }

  /**
   * @return void
   */
  protected function createEmailContentTemplateType() {
    // @todo TBD.
  }

}
