<?php

namespace Drupal\email_content_templates;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of email content template type entities.
 *
 * @see \Drupal\email_content_templates\Entity\EmailContentTemplateType
 */
class EctTypeListBuilder extends ConfigEntityListBuilder {


  /**
   * The email info manager.
   *
   * @var \Drupal\email_content_templates\EctMailPluginManager
   */
  protected $emailInfoManager;

  /**
   * Constructs a new EctListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\email_content_templates\EctMailPluginManager $email_info_manager
   *   The email info manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, EctMailPluginManager $email_info_manager) {
    parent::__construct($entity_type, $storage);
    $this->emailInfoManager = $email_info_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('plugin.manager.ect_mail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'email_content_template_types_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['description'] = $this->t('Description');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\email_content_templates\Entity\EmailContentTemplateTypeInterface $entity */
    $row['label'] = $entity->label();
    $row['description'] = $entity->getDescription();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t(
      'No email content template types available. <a href=":link">Add new type</a>.',
      [
        ':link' => Url::fromRoute('entity.email_content_template_type.add_form')
          ->toString(),
      ]
    );

    return $build;
  }

}
