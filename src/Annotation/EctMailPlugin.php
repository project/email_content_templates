<?php

namespace Drupal\email_content_templates\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for EmailContentTemplateInfo plugins.
 *
 * @Annotation
 */
class EctMailPlugin extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Optional short description text for this plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Category name for this plugin.
   *
   * @var string
   */
  public $category;
}
