<?php

namespace Drupal\email_content_templates;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\email_content_templates\Plugin\Derivative\MailPluginDeriver;

/**
 * EmailContentTemplatesInfo plugin manager.
 */
class EctMailPluginManager extends DefaultPluginManager {

  /**
   * Array of derivers.
   *
   * @var array
   */
  protected $derivatives = [];

  /**
   * Constructs EctMailPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/EctMail',
      $namespaces,
      $module_handler,
      'Drupal\email_content_templates\Plugin\EctMail\EctMailPluginInterface',
      'Drupal\email_content_templates\Annotation\EctMailPlugin'
    );
    $this->alterInfo('ect_mail_plugin_info');
    $this->setCacheBackend($cache_backend, 'ect_mail_plugins');
    $this->derivatives[] = new MailPluginDeriver($module_handler);
  }

  /**
   * {@inheritdoc}
   */
  protected function findDefinitions() {
    $definitions = parent::findDefinitions();
    foreach ($this->derivatives as $derivative) {
      $definitions += $derivative->getDerivativeDefinitions([]);
    }
    return $definitions;
  }

}
