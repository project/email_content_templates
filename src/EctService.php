<?php

namespace Drupal\email_content_templates;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\email_content_templates\Entity\EmailContentTemplateInterface;
use Drupal\email_content_templates\Plugin\EctMail\EctMailPluginInterface;
use Drupal\token\TokenInterface;

/**
 * Provides Email Preparation functionality.
 */
class EctService implements EctServiceInterface, TrustedCallbackInterface {

  /**
   * The email plugin manager.
   *
   * @var \Drupal\email_content_templates\EctMailPluginManager
   */
  protected $emailPluginManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The token service.
   *
   * @var \Drupal\token\TokenInterface
   */
  protected $token;

  /**
   * The ect config settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The twig environment.
   *
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  protected $twig;

  /**
   * Collected token data.
   *
   * @var array
   */
  protected $tokenData;

  /**
   * Constructs a new EctService.
   */
  public function __construct(EctMailPluginManager $email_plugin_manager, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, RendererInterface $renderer, TokenInterface $token, TwigEnvironment $twig) {
    $this->emailPluginManager = $email_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->config = $config_factory->get('email_content_templates.settings');
    $this->moduleHandler = $module_handler;
    $this->renderer = $renderer;
    $this->token = $token;
    $this->twig = $twig;
  }

  /**
   * {@inheritdoc}
   */
  public function findTemplatesForMail(array $mail) {
    if (!$this->config->get('enabled')) {
      // Module is disabled. Do nothing.
      return [];
    }

    $plugin_id = $mail['module'] . '-' . $mail['key'];

    // Get all enabled template types.
    $types = \Drupal::entityTypeManager()
      ->getStorage('email_content_template_type')->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', 1)
      ->execute();

    $template_storage = \Drupal::entityTypeManager()->getStorage('email_content_template');
    // Load all enabled templates using plugin of current mail key
    // sorted by weight.
    /** @var \Drupal\email_content_templates\Entity\EmailContentTemplateInterface[] $templates */
    $results = $template_storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', 1)
      ->condition('bundle', $types, 'IN')
      ->condition('email_plugin', $plugin_id)
      ->sort('weight')
      ->execute();
    if (!empty($results)) {
      // Filter possible templates to use.
      foreach ($results as $key => $template_id) {
        /** @var \Drupal\email_content_templates\Entity\EmailContentTemplateInterface $template */
        $template = $template_storage->load($template_id);
        if ($template->applies($mail)) {
          $results[$key] = $template;
        }
        else {
          unset($results[$key]);
        }
      }
    }

    // Allow modules to alter the email content templates results list.
    $this->moduleHandler->alter('email_content_templates', $results, $mail);
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareMessage(&$mail, EmailContentTemplateInterface $template, EctMailPluginInterface $plugin = NULL) {
    if (empty($plugin)) {
      $plugin = $this->emailPluginManager->createInstance($template->getEmailPluginId());
    }

    if ($template->hasTranslation($this->languageManager->getCurrentLanguage()->getId())) {
      // Load translation.
      $template = $template->getTranslation($this->languageManager->getCurrentLanguage()->getId());
    }

    // Collect variables for rendering from plugin.
    $plugin->preRenderAlterMail($template, $mail);
    $params = $mail['params'];
    // Allow modules to alter variables for rendering.
    $this->moduleHandler->alter('mail_plugin_' . str_replace('-', '_', $plugin->getPluginId()), $params);

    $body = $template->get('body')->getValue();
    /** @var \Drupal\filter\FilterFormatInterface $format */
    $format = $this->entityTypeManager->getStorage('filter_format')->load($body[0]['format']);
    $filters = $format->filters();
    $html_mail = TRUE;
    if ($filters->has('filter_html_escape')) {
      $filter = $filters->get('filter_html_escape');
      $html_mail = !$filter->status;
    }
    if ($html_mail) {
      $mail['headers']['Content-Type'] = 'text/html';

      $render = $this->entityTypeManager->getViewBuilder($template->getEntityTypeId())->view($template, 'email');
      $rendered_body = $this->renderer->render($render);
      $body[0]['value'] = $rendered_body;
    }
    $mail['params']['email_content_template'] = $template;
    $mail['body'][0] = $this->token->replace($body[0]['value'], $this->getTokenData($params, $template, $plugin), $plugin->getTokenOptions());
    $mail['subject'] = $this->token->replace($template->getSubject(), $this->getTokenData($params, $template, $plugin), $plugin->getTokenOptions());
    $plugin->postRenderAlterMail($template, $mail);
    $mail['#email_content_template'] = $template;
    // Wrap body for rendering.
    $mail['body'][0] = [
      '#type' => 'processed_text',
      '#text' => $mail['body'][0],
      '#format' => $body[0]['format'],
      '#pre_render' => [
        // Preserve the default pre_render from the element.
        ['Drupal\filter\Element\ProcessedText', 'preRenderText'],
        [$this, 'preRenderVariables'],
      ],
      '#context' => $params,
    ];
  }

  /**
   * Helper function to build the token data for replacements.
   *
   * @param array $mail_params
   *   The mail params.
   * @param \Drupal\email_content_templates\Entity\EmailContentTemplateInterface|NULL $template
   *   The to use template.
   * @param \Drupal\email_content_templates\Plugin\EctMail\EctMailPluginInterface|NULL $plugin
   *   The plugin.
   *
   * @return array
   *   The token data array.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getTokenData(array $mail_params, EmailContentTemplateInterface $template = NULL, EctMailPluginInterface $plugin = NULL) {
      $this->tokenData = $mail_params;
      if (empty($plugin) && !empty($template)) {
        $plugin = $this->emailPluginManager->createInstance($template->getEmailPluginId());
      }
      if (!empty($plugin)) {
        $param_token_map = $plugin->paramTokenMap();
        if (!empty($param_token_map)) {
          foreach ($param_token_map as $param => $token) {
            if (isset($mail_params[$param])) {
              $this->tokenData[$token] = $mail_params[$param];
            }
          }
        }
      }

    return $this->tokenData;
  }

  /**
   * Pre-render callback for replacing twig variables.
   */
  public function preRenderVariables(array $body) {
    $body['#markup'] = $this->twig->renderInline($body['#text'], $body['#context']);
    return $body;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRenderVariables'];
  }

}
