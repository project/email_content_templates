<?php

namespace Drupal\email_content_templates\Entity;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the email content template entity class.
 *
 * @ContentEntityType(
 *   id = "email_content_template",
 *   label = @Translation("Email template"),
 *   label_collection = @Translation("Email templates"),
 *   bundle_label = @Translation("Email content template type"),
 *   handlers = {
 *     "view_builder" = "Drupal\email_content_templates\EctViewBuilder",
 *     "list_builder" = "Drupal\email_content_templates\EctListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "add" = "Drupal\email_content_templates\Form\EmailContentTemplateForm",
 *       "edit" = "Drupal\email_content_templates\Form\EmailContentTemplateForm",
 *       "duplicate" = "Drupal\email_content_templates\Form\EmailContentTemplateForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *        "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *      },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *   },
 *   base_table = "email_content_template",
 *   data_table = "email_content_template_field_data",
 *   revision_table = "email_content_template_revision",
 *   revision_data_table = "email_content_template_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer email_content_template",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "bundle" = "bundle",
 *     "label" = "title",
 *     "owner" = "uid",
 *     "published" = "status",
 *     "uuid" = "uuid",
 *     "weight" = "weight",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/email-content-template/add/{email_content_template_type}",
 *     "add-page" = "/admin/content/email-content-template/add",
 *     "canonical" = "/email_content_template/{email_content_template}",
 *     "edit-form" = "/admin/content/email-content-template/{email_content_template}/edit",
 *     "duplicate-form" = "/admin/content/email-content-template/{email_content_template}/duplicate",
 *     "delete-form" = "/admin/content/email-content-template/{email_content_template}/delete",
 *     "delete-multiple-form" = "/admin/content/email-content-templates/delete",
 *     "collection" = "/admin/content/email-content-template"
 *   },
 *   bundle_entity_type = "email_content_template_type",
 *   field_ui_base_route = "entity.email_content_template_type.edit_form"
 * )
 */
class EmailContentTemplate extends EditorialContentEntityBase implements EmailContentTemplateInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return (int) $this->get('weight')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubject() {
    return $this->get('subject')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSubject($subject) {
    $this->set('subject', $subject);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);
    // Add ownership fields.
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields[$entity_type->getKey('published')]
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => -20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields[$entity_type->getKey('owner')]
      ->setLabel(t('Author'))
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the template entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['email_plugin'] = BaseFieldDefinition::create('list_string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Email plugin'))
      ->setDescription(t('The mail plugin used on this template.'))
      ->setSetting('allowed_values_function', [static::class, 'getAllowedEmailPlugins'])
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['subject'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Subject'))
      ->setDescription(t('The subject of the mail.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['body'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Body'))
      ->setDescription(t('The body of the mail.'))
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['conditions'] = BaseFieldDefinition::create('map')
      //->setTranslatable(TRUE)
      ->setLabel(t('Conditions'))
      ->setDescription(t('The condition settings.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the template was created.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setDescription(t('The time that the template was last edited.'));

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of this price list in relation to other price lists.'))
      ->setDefaultValue(0);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmailPluginId() {
    return $this->get('email_plugin')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmailPlugin() {
    if (!empty($this->getEmailPluginId())) {
      try {
        return \Drupal::service('plugin.manager.ect_mail')->createInstance($this->getEmailPluginId());
      }
      catch (PluginNotFoundException $e) {
        // Do nothing.
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(array $message): bool {
    if ($plugin = $this->getEmailPlugin()) {
      return $plugin->applies($this, $message);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleEntity() {
    return \Drupal::entityTypeManager()->getStorage('email_content_template_type')->load($this->bundle());
  }

  /**
   * Callback to build list of available email plugins for this template.
   *
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $definition
   *   Field storage definition context.
   * @param $entity
   *   The current entity.
   * @param $cacheable
   *   Indicator if this is cacheable.
   *
   * @return array
   *   Allowed email plugin options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getAllowedEmailPlugins($definition, $entity, $cacheable) {
    /** @var self $entity */
    /** @var \Drupal\email_content_templates\Entity\EmailContentTemplateTypeInterface $bundle */
    $bundle = \Drupal::entityTypeManager()->getStorage('email_content_template_type')->load($entity->bundle());
    $plugins = $bundle->getEmailPlugins();
    $options = [];
    foreach ($plugins as $plugin) {
      $plugin_definition = $plugin->getPluginDefinition();
      if (empty($plugin_definition['category'])) {
        $options[$plugin->getPluginId()] = $plugin->label();
      }
      else {
        $options[$plugin_definition['category']][$plugin_definition['id']] = $plugin_definition['label'];
      }

    }
    asort($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function setCondtions(array $conditions) {
    $this->set('conditions', $conditions);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions() {
    return !$this->get('conditions')->isEmpty() ? $this->get('conditions')->first()->getValue() : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionPluginCollection() {
    if (!isset($this->conditionPluginCollection)) {
      $this->conditionPluginCollection = new ConditionPluginCollection($this->conditionPluginManager(), $this->getConditions());
    }
    return $this->conditionPluginCollection;
  }

  /**
   * Gets the condition plugin manager.
   *
   * @return \Drupal\Core\Executable\ExecutableManagerInterface
   *   The condition plugin manager.
   */
  protected function conditionPluginManager() {
    if (!isset($this->conditionPluginManager)) {
      $this->conditionPluginManager = \Drupal::service('plugin.manager.condition');
    }
    return $this->conditionPluginManager;
  }

}
