<?php

namespace Drupal\email_content_templates\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an email content template entity type.
 */
interface EmailContentTemplateInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the email content template title.
   *
   * @return string
   *   Title of the email content template.
   */
  public function getTitle();

  /**
   * Sets the email content template title.
   *
   * @param string $title
   *   The email content template title.
   *
   * @return self
   */
  public function setTitle($title);

  /**
   * Gets the email content template creation timestamp.
   *
   * @return int
   *   Creation timestamp of the email content template.
   */
  public function getCreatedTime();

  /**
   * Sets the email content template creation timestamp.
   *
   * @param int $timestamp
   *   The email content template creation timestamp.
   *
   * @return self
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the weight.
   *
   * @return int
   *   The weight.
   */
  public function getWeight();

  /**
   * Sets the weight.
   *
   * @param int $weight
   *   The desired weight.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Returns the subject.
   *
   * @return string
   *   The subject.
   */
  public function getSubject();

  /**
   * Sets the subject.
   *
   * @param string $subject
   *   The subject.
   *
   * @return $this
   */
  public function setSubject($subject);

  /**
   * Returns the email plugin id.
   *
   * @return string
   *   The email plugin id.
   */
  public function getEmailPluginId();

  /**
   * Returns the email plugin.
   *
   * May return null, in case plugin is not set or missing.
   *
   * @return \Drupal\email_content_templates\Plugin\EctMail\EctMailPluginInterface|null
   *   The email plugin or null.
   */
  public function getEmailPlugin();

  /**
   * Checks if this template should be used.
   *
   * @param array $message
   *   The message.
   *
   * @return bool
   *   Whether the template should be used.
   */
  public function applies(array $message): bool;

  /**
   * Returns the entity bundle type.
   *
   * @return \Drupal\email_content_templates\Entity\EmailContentTemplateTypeInterface
   *   The entity bundle type.
   */
  public function getBundleEntity();

  /**
   * Sets the condition settings.
   *
   * @param array $conditions
   *   The condition settings.
   *
   * @return $this
   */
  public function setCondtions(array $conditions);

  /**
   * Gets condition settings.
   *
   * @return array
   *   The condition settings.
   */
  public function getConditions();

  /**
   * Gets condition collection for this plugin.
   *
   * @return \Drupal\Core\Condition\ConditionInterface[]|\Drupal\Core\Condition\ConditionPluginCollection
   *   An array or collection of configured condition plugins.
   */
  public function getConditionPluginCollection();

}
