<?php

namespace Drupal\email_content_templates\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;

/**
 * Defines the email content template type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "email_content_template_type",
 *   label = @Translation("Email content template type"),
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "add" = "Drupal\email_content_templates\Form\EmailContentTemplateTypeForm",
 *       "edit" = "Drupal\email_content_templates\Form\EmailContentTemplateTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\email_content_templates\EctTypeListBuilder",
 *     "local_task_provider" = {
 *        "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *      },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer email_content_template_type",
 *   bundle_of = "email_content_template",
 *   config_prefix = "email_content_template_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "weight" = "weight",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/email_content_template_types/add",
 *     "edit-form" = "/admin/structure/email_content_template_types/manage/{email_content_template_type}",
 *     "delete-form" = "/admin/structure/email_content_template_types/manage/{email_content_template_type}/delete",
 *     "collection" = "/admin/structure/email_content_template_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "weight",
 *     "uuid",
 *     "status",
 *     "description",
 *     "default_subject",
 *     "default_body",
 *     "default_body_format",
 *     "email_plugins",
 *     "allowed_text_formats"
 *   }
 * )
 */
class EmailContentTemplateType extends ConfigEntityBundleBase implements EmailContentTemplateTypeInterface {

  /**
   * The machine name of this email content template type.
   *
   * @var string
   */
  protected $id;

  /**
   * The allowed text formats.
   *
   * @var string
   */
  protected $allowed_text_formats;

  /**
   * The human-readable name of the type.
   *
   * @var string
   */
  protected $label;

  /**
   * Flag if templates of this type are enabled.
   *
   * @var bool
   */
  protected $status;

  /**
   * The weight of this template type.
   *
   * @var int
   */
  protected $weight;

  /**
   * The ids of allowed email plugins.
   *
   * @var array
   */
  protected $email_plugins = [];

  /**
   * The bundle description.
   *
   * @var string
   */
  protected $description;

  /**
   * The default title.
   *
   * @var string
   */
  protected $default_subject;

  /**
   * The default body.
   *
   * @var string
   */
  protected $default_body;

  /**
   * The default body format.
   *
   * @var string
   */
  protected $default_body_format;

  /**
   * {@inheritdoc}
   */
  public function setEmailPlugins(array $email_plugins) {
    $this->email_plugins = $email_plugins;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmailPluginIds() {
    return $this->email_plugins;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmailPlugins() {
    $plugins = [];
    $email_templates_plugin_manager = \Drupal::service('plugin.manager.ect_mail');
    foreach ($this->getEmailPluginIds() as $plugin_id) {
      $plugins[] = $email_templates_plugin_manager->createInstance($plugin_id);
    }
    return $plugins;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultSubject($default_subject) {
    $this->default_subject = $default_subject;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSubject() {
    return $this->default_subject;
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultBody($default_body) {
    $this->default_body = $default_body;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultBody() {
    return $this->default_body;
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultBodyFormat($default_body_format) {
    $this->default_body_format = $default_body_format;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultBodyFormat() {
    return $this->default_body_format;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowedTextFormats() {
    return $this->allowed_text_formats ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function setAllowedTextFormats($allowed_text_formats) {
    $this->allowed_text_formats =  $allowed_text_formats;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight');
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

}
