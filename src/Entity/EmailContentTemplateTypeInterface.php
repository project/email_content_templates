<?php

namespace Drupal\email_content_templates\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

interface EmailContentTemplateTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

  /**
   * Sets the allowed email plugins.
   *
   * @param array $email_plugins
   *   List of plugin ids.
   */
  public function setEmailPlugins(array $email_plugins);

  /**
   * Returns the allowed email plugin ids.
   *
   * @return array
   *   The email plugin ids.
   */
  public function getEmailPluginIds();

  /**
   * Returns the email plugins.
   *
   * @return \Drupal\email_content_templates\Plugin\EctMail\EctMailPluginInterface[]
   *   The email plugins.
   */
  public function getEmailPlugins();

  /**
   * Sets the default template title.
   *
   * @param string $default_subject
   *   The default title.
   */
  public function setDefaultSubject($default_subject);

  /**
   * Returns the default template title.
   *
   * @return string
   *   The default template title.
   */
  public function getDefaultSubject();

  /**
   * Sets the default template body.
   *
   * @param string $default_body
   *   The default body.
   */
  public function setDefaultBody($default_body);

  /**
   * Returns the default template body.
   *
   * @return string
   *   The default template body.
   */
  public function getDefaultBody();

  /**
   * Sets the default template body format.
   *
   * @param string $default_body_format
   *   The default body format.
   */
  public function setDefaultBodyFormat($default_body_format);

  /**
   * Returns the default template body.
   *
   * @return string
   *   The default template body format.
   */
  public function getDefaultBodyFormat();

  /**
   * Returns the weight.
   *
   * @return int
   *   The weight of this role.
   */
  public function getWeight();

  /**
   * Sets the weight to the given value.
   *
   * @param int $weight
   *   The desired weight.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Sets the allowed text formats.
   *
   * @param $allowed_text_formats array
   *   The allowed text formats
   */
  public function setAllowedTextFormats($allowed_text_formats);

  /**
   * Gets the allowed text formats.
   *
   * @return array
   *   The allowed text formats
   */
  public function getAllowedTextFormats();

}
