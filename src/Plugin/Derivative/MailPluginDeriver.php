<?php

namespace Drupal\email_content_templates\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\email_content_templates\Plugin\EctMail\EctUserMailPlugin;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MailPluginDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new MailPluginDeriver object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $user_mail_plugins = [
      'user-cancel_confirm' => $this->t('User cancel confirm'),
      'user-password_reset' => $this->t('User password reset'),
      'user-register_admin_created' => $this->t('User register admin created'),
      'user-register_no_approval_required' => $this->t('User register no approval required'),
      'user-register_pending_approval' => $this->t('User register pending approval'),
      'user-register_pending_approval_admin' => $this->t('User register pending approval admin notification'),
      'user-status_activated' => $this->t('User status activated'),
      'user-status_blocked' => $this->t('User status blocked'),
      'user-status_canceled' => $this->t('User status canceled'),
    ];

    if ($this->moduleHandler->moduleExists('user_registrationpassword')) {
      $user_mail_plugins['user_registrationpassword-register_confirmation_with_pass'] = $this->t('User register no approval required, password is set');
    }

    foreach ($user_mail_plugins as $id => $label) {
      $this->derivatives[$id] = $base_plugin_definition;
      $this->derivatives[$id]['id'] = $id;
      $this->derivatives[$id]['label'] = $label;
      $this->derivatives[$id]['category'] = 'User';
      $this->derivatives[$id]['class'] = EctUserMailPlugin::class;
    }

    return $this->derivatives;
  }

}
