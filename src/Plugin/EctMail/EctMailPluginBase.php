<?php

namespace Drupal\email_content_templates\Plugin\EctMail;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\email_content_templates\Entity\EmailContentTemplateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for email_content_templates_info plugins.
 */
abstract class EctMailPluginBase extends PluginBase implements EctMailPluginInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $conditionsManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $language;

  /**
   * The plugin context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected $contextHandler;

  /**
   * The context manager service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected ContextRepositoryInterface $contextRepository;

  /**
   * The condition settings for this plugin.
   *
   * @var array
   */
  protected $conditions = [];

  public function getContextMapping() {
    return [];
  }

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The mail module.
   *
   * @var string
   */
  protected $mailModule;

  /**
   * The mail key.
   *
   * @var string
   */
  protected $mailKey;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
    [$instance->mailModule, $instance->mailKey] = explode('-', $plugin_id,2);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->conditionsManager = $container->get('plugin.manager.condition');
    $instance->language = $container->get('language_manager');
    $instance->contextHandler = $container->get('context.handler');
    $instance->contextRepository = $container->get('context.repository');
    $instance->moduleHandler = $container->get('module_handler');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function paramTokenMap() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityContextIds():array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getContexts($mail = []):array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenOptions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function applies(EmailContentTemplateInterface $template, array $message): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function preRenderAlterMail(EmailContentTemplateInterface $template, array &$message): void {
  }

  /**
   * {@inheritdoc}
   */
  public function postRenderAlterMail(EmailContentTemplateInterface $template, array &$message): void {
    $this->moduleHandler->alter('mail', $message);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConditionsForm(EmailContentTemplateInterface $template, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMailVariables() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultText() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'settings' => $this->getPluginCollection(),
      'conditions' => $this->getVisibilityConditions(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTestForm(FormStateInterface $form_state): array {
    $param_token_map = $this->paramTokenMap();
    $form = [];
    foreach ($param_token_map as $param_name => $entity_type) {
      $form[$param_name] = [
        '#title' => $param_name,
        '#type' => 'entity_autocomplete',
        '#target_type' => $entity_type,
        '#required' => TRUE,

      ];
      if ($entity_id = $form_state->getValue($param_name)) {
        if ($entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id)) {
          $form[$param_name]['#default_value'] = $entity;
        }
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getMailModule() {
    return $this->mailModule;
  }

  /**
   * {@inheritdoc}
   */
  public function getMailKey() {
    return $this->mailKey;
  }

}
