<?php

namespace Drupal\email_content_templates\Plugin\EctMail;

use Drupal\email_content_templates\Annotation\EctMailPlugin;

/**
 * Plugin implementation of the email_content_templates_info.
 *
 * @EctMailPlugin(
 *   id = "email_content_templates-empty_template",
 *   label = @Translation("Empty template"),
 * )
 */
class EmptyTemplate extends EctMailPluginBase {

}
