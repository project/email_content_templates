<?php

namespace Drupal\email_content_templates\Plugin\EctMail;

/**
 * Plugin implementation of the update status notification.
 *
 * @EctMailPlugin(
 *   id = "update-status_notify",
 *   label = @Translation("Update status notify"),
 * )
 */
class UpdateStatusNotify extends EctMailPluginBase {

}
