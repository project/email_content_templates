<?php

namespace Drupal\email_content_templates\Plugin\EctMail;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Component\Plugin\Exception\MissingValueContextException;
use Drupal\Core\Condition\ConditionAccessResolverTrait;
use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\email_content_templates\Entity\EmailContentTemplateInterface;
use Drupal\user\Entity\User;

/**
 * Abstract base class for user related mail plugins.
 */
class EctUserMailPlugin extends EctMailPluginBase {

  use ConditionAccessResolverTrait;

  /**
   * The config name containing the original mail text settings.
   *
   * @var string
   */
  protected $config_name = 'user.mail';

  /**
   * {@inheritdoc}
   */
  public function paramTokenMap() {
    return [
      'account' => 'user',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenOptions() {
    return [
      'callback' => 'user_mail_tokens',
      'clear' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityContextIds():array {
    return ['user'];
  }

  /**
   * {@inheritdoc}
   */
  public function getContexts($mail = []):array {
    return [
      'user' => EntityContext::fromEntity($mail['params']['account']),
      'account' => EntityContext::fromEntity($mail['params']['account'])
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConditionsForm(EmailContentTemplateInterface $template, FormStateInterface $form_state) {
    // Adding conditions here.
    // User role and page path seem to be the only meaningful conditions
    // at this point, if any. Not sure, actually.
    $form['conditions'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Conditions'),
      '#parents' => ['conditions'],
    ];

    $conditions = $template->getConditions();
    $contexts = [];
    foreach ($this->getEntityContextIds() as $context_id) {
      $contexts[$context_id] = EntityContext::fromEntityTypeId($context_id);
    }
    $definitions = $this->conditionsManager->getFilteredDefinitions('email_content_template', $contexts, ['email_content_template' => $template]);
    foreach ($definitions as $condition_id => $definition) {
      switch ($condition_id) {
        case 'current_theme':
        case 'language':
        case 'response_status':
          // Currently unsupported conditions.
          continue 2;
      }

      if (str_contains($condition_id, 'entity_bundle:')) {
        // Hide entity bundle related conditions (node/taxonomy).
        continue;
      }

      /** @var \Drupal\Core\Condition\ConditionInterface $condition */
      $condition = $this->conditionsManager->createInstance($condition_id, $conditions[$condition_id] ?? []);
      $form_state->set(['conditions', $condition_id], $condition);
      $condition_form = $condition->buildConfigurationForm([], $form_state);
      $condition_form['context_mapping']['#access'] = FALSE;
      $condition_form['#type'] = 'details';
      $condition_form['#title'] = $condition->getPluginDefinition()['label'];
      $condition_form['#group'] = 'conditions';
      $form[$condition_id] = $condition_form;
    }

    if (isset($form['user_role'])) {
      $form['user_role']['#title'] = $this->t('Roles');
      unset($form['user_role']['roles']['#description']);
    }

    if (isset($form['request_path'])) {
      $form['request_path']['#title'] = $this->t('Pages');
      $form['request_path']['negate']['#type'] = 'radios';
      $form['request_path']['negate']['#default_value'] = (int) $form['request_path']['negate']['#default_value'];
      $form['request_path']['negate']['#title_display'] = 'invisible';
      $form['request_path']['negate']['#options'] = [
        $this->t('Show for the listed pages'),
        $this->t('Hide for the listed pages'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(EmailContentTemplateInterface $template, array $message): bool {
    $plugin = $template->getEmailPlugin();
    if (empty($plugin)) {
      return FALSE;
    }

    $condition_settings = $template->getConditions();
    if (isset($condition_settings['conditions__active_tab'])) {
      unset($condition_settings['conditions__active_tab']);
    }
    if (count($condition_settings) === 0) {
      // Templates without conditions always apply.
      return TRUE;
    }

    foreach ($condition_settings as $condition_id => $settings) {
      // Required by DefaultLazyPluginCollection.
      $condition_settings[$condition_id]['id'] = $condition_id;
    }

    $conditions = [];
    $conditions_collection = new ConditionPluginCollection($this->conditionsManager, $condition_settings);
    $contexts = $this->getContexts($message);

    /**
     * @var string $condition_id
     * @var \Drupal\Core\Condition\ConditionPluginBase $condition
     */
    foreach ($conditions_collection as $condition_id => $condition) {
      $conditions[$condition_id] = $condition;
      $this->contextHandler->applyContextMapping($condition, $contexts);
    }




    return $this->resolveConditions($conditions, 'and');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultText() {
    $config = \Drupal::config($this->config_name);
    $plugin_id = explode('-', $this->getPluginId(), 2);
    return $config->get($plugin_id[1])['body'];
  }

}
