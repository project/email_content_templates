<?php

namespace Drupal\email_content_templates\Plugin\EctMail;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\ObjectWithPluginCollectionInterface;
use Drupal\email_content_templates\Entity\EmailContentTemplateInterface;

/**
 * Interface for email_content_templates_info plugins.
 */
interface EctMailPluginInterface extends PluginInspectionInterface, ObjectWithPluginCollectionInterface, ContainerFactoryPluginInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the information so hook_mail parameters can be mapped to tokens.
   *
   * Format: [param_from_mail => token_data_name]
   *
   * @return array
   *   The map.
   */
  public function paramTokenMap();

  /**
   * Return options array for token replacements.
   *
   * @return array
   *   The options.
   *
   * @see \Drupal\Core\Utility\Token::replace
   */
  public function getTokenOptions();

  /**
   * Returns the form used for testing the template.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form array.
   */
  public function getTestForm(FormStateInterface $form_state): array;

  /**
   * Checks if the given template should be used.
   *
   * @param \Drupal\email_content_templates\Entity\EmailContentTemplateInterface $template
   *   The template.
   * @param array $message
   *   The message.
   *
   * @return bool
   *   Whether the template should be used or not.
   */
  public function applies(EmailContentTemplateInterface $template, array $message): bool;

  /**
   * Alter the mail message before rendering the body.
   *
   * Can be used to add variables for rendering.
   *
   * @param \Drupal\email_content_templates\Entity\EmailContentTemplateInterface $template
   *   The template.
   * @param array $message
   *   The data from hook_mail().
   */
  public function preRenderAlterMail(EmailContentTemplateInterface $template, array &$message): void;

  /**
   * Alter the mail message after rendering the body.
   *
   * @param \Drupal\email_content_templates\Entity\EmailContentTemplateInterface $template
   *   The template.
   * @param array $message
   *   The data from hook_mail().
   */
  public function postRenderAlterMail(EmailContentTemplateInterface $template, array &$message): void;

  /**
   * Builds the conditions form.
   *
   * @param \Drupal\email_content_templates\Entity\EmailContentTemplateInterface $template
   *   The template using the conditions.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form build.
   */
  public function buildConditionsForm(EmailContentTemplateInterface $template, FormStateInterface $form_state);

  /**
   * Returns variables information.
   *
   * Mail plugins may provide information about available variables, which can
   * be used inside the template body.
   *
   * @return array
   *   List of available variables. Format: variable name => description.
   */
  public function getMailVariables();

  /**
   * Returns the default body text for this mail plugin.
   *
   * Mail plugins may offer a default text, that can be imported into the body
   * of the mail template so users can modify the content more quickly.
   *
   * Note that returning HTML may not work reliably well withing the ckeditor.
   * In that case use a text format without an editor instead.
   *
   * @return string
   *   The default body text.
   */
  public function getDefaultText();

  /**
   * Returns contexts for this plugin.
   *
   * @return array
   *   The context ids.
   */
  public function getEntityContextIds():array;

  /**
   * Returns contexts for this plugin.
   *
   * @return array
   *   The context ids.
   */
  public function getContexts($mail = []):array;

  /**
   * Returns the module of mail.
   *
   * @return string
   *   The mail module.
   */
  public function getMailModule();

  /**
   * Returns the key of mail.
   *
   * @return string
   *   The mail key.
   */
  public function getMailKey();
}
