<?php

namespace Drupal\email_content_templates\Form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\email_content_templates\Entity\EmailContentTemplateInterface;
use Drupal\email_content_templates\Entity\EmailContentTemplateTypeInterface;
use Drupal\entity\Form\EntityDuplicateFormTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the email content template entity edit forms.
 */
class EmailContentTemplateForm extends ContentEntityForm {

  use EntityDuplicateFormTrait;

  /**
   * The current template entity.
   *
   * @var \Drupal\email_content_templates\Entity\EmailContentTemplateInterface
   */
  protected $entity;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The context repository service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $conditionsManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
    );

    $instance->dateFormatter = $container->get('date.formatter');
    $instance->contextRepository = $container->get('context.repository');
    $instance->conditionsManager = $container->get('plugin.manager.condition');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $result = $entity->save();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments;

    if ($result == SAVED_NEW) {
      $this->messenger()
        ->addStatus($this->t('New email content template %label has been created.', $message_arguments));
          }
    else {
      $this->messenger()
        ->addStatus($this->t('The email content template %label has been updated.', $message_arguments));
      $this->logger('email_content_templates')
        ->notice('Updated new email content template %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.email_content_template.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    /** @var \Drupal\email_content_templates\Entity\EmailContentTemplateInterface $entity */
    $entity = parent::getEntityFromRouteMatch($route_match, $entity_type_id);

    $bundle = $entity->getBundleEntity();
    if ($bundle instanceof EmailContentTemplateTypeInterface) {
      // Set default values if empty.
      if ($entity->get('subject')->isEmpty()) {
        $entity->setSubject($bundle->getDefaultSubject());
      }
      if ($entity->get('body')->isEmpty()) {
        $entity->set('body', [
          'value' => $bundle->getDefaultBody(),
          'format' => $bundle->getDefaultBodyFormat(),
        ]);
      }
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->setTemporaryValue('gathered_contexts', $this->contextRepository->getAvailableContexts());

    $form = parent::buildForm($form, $form_state);
    $template = $this->entity;
    if (isset($form['email_plugin']['widget'])) {
      $options = $form['email_plugin']['widget']['#options'];
      $flatted_options = [];
      foreach ($options as $group_id => $group) {
        if (is_array($group)) {
          foreach ($group as $option_id => $option) {
            $flatted_options[] = $option_id;
          }
        } else if ($group_id !== '_none') {
          $flatted_options[] = $group_id;
        }
      }
      if (count($flatted_options) === 1) {
        $form['email_plugin']['widget']['#default_value'] = $flatted_options;
        $form['email_plugin']['#attributes']['class'][] = 'hidden';
      }
    }
    $this->showTemplateVariables($form, $form_state, $this->entity);

    $allowed_formats = $template->getBundleEntity()->getAllowedTextFormats();
    if (count($allowed_formats) !== 0) {
      $form['body']['widget'][0]['#allowed_formats'] = $allowed_formats;
    }
    $storage = $form_state->getStorage();
    if ($storage['langcode'] === $storage['entity_default_langcode']) {
      // Conditions are currently hidden while in translation form.
      $form['email_plugin']['widget']['#ajax'] = [
        // Add ajax handler on email plugin field.
        'callback' => '::ajaxUpdateForm',
        'wrapper' => 'form-wrapper',
      ];
      $form['conditions'] = [
        '#prefix' => '<div id="conditions-wrapper">',
        '#suffix' => '</div>',
        '#weight' => 50,
        '#tree' => TRUE,
      ];

      $this->addConditions($form, $form_state, $this->entity);
    }

    $form['meta'] = [
      '#type' => 'details',
      '#group' => 'advanced',
      '#weight' => -10,
      '#title' => $this->t('Status'),
      '#attributes' => ['class' => ['entity-meta__header']],
      '#tree' => TRUE,
      '#access' => $this->currentUser()->hasPermission('administer email_content_template'),
    ];
    $form['meta']['published'] = [
      '#type' => 'item',
      '#markup' => $template->isPublished() ? $this->t('Published') : $this->t('Not published'),
      '#access' => !$template->isNew(),
      '#wrapper_attributes' => ['class' => ['entity-meta__title']],
    ];
    $form['meta']['changed'] = [
      '#type' => 'item',
      '#title' => $this->t('Last saved'),
      '#markup' => !$template->isNew() ? $this->dateFormatter->format($template->getChangedTime(), 'short') : $this->t('Not saved yet'),
      '#wrapper_attributes' => ['class' => ['entity-meta__last-saved']],
    ];
    $form['meta']['author'] = [
      '#type' => 'item',
      '#title' => $this->t('Author'),
      '#markup' => $template->getOwner()->getAccountName(),
      '#wrapper_attributes' => ['class' => ['entity-meta__author']],
    ];
    if (isset($form['uid'])) {
      $form['author'] = [
        '#type' => 'details',
        '#title' => $this->t('Authoring information'),
        '#group' => 'advanced',
        '#weight' => 90,
        '#optional' => TRUE,
      ];
      $form['uid']['#group'] = 'author';
      if (isset($form['created'])) {
        $form['created']['#group'] = 'author';
      }
    }
    if ($this->moduleHandler->moduleExists('token')) {
      $email_plugin = $this->getCurrentPlugin($form_state, $template);
      if (!empty($email_plugin) && $email_plugin->getPluginId() !== '_none') {
        try {
          $token_map = $email_plugin->paramTokenMap();
          if (!empty($token_map)) {
            $form['body']['token_link'] = [
              '#theme' => 'token_tree_link',
              '#token_types' => array_values($token_map),
              '#global_types' => TRUE,
            ];
          }
        }
        catch (PluginNotFoundException $exception) {
          // Plugin not found.
        }
      }
    }

    return [
      '#prefix' => '<div id="form-wrapper">',
      '#suffix' => '</div>'
    ] + $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Submit conditions.
    $condition_values = $form_state->getValue('conditions');
    if (isset($condition_values['conditions__active_tab'])) {
      unset($condition_values['conditions__active_tab']);
    }
    if (!empty($condition_values)) {
      foreach ($condition_values as $condition_id => $values) {
        if (!is_array($values)) {
          continue;
        }

        $condition = $form_state->get(['conditions', $condition_id]);
        $condition->submitConfigurationForm($form['conditions'][$condition_id], SubformState::createForSubform($form['conditions'][$condition_id], $form, $form_state));
        $condition_configuration = $condition->getConfiguration();
        $condition_values[$condition_id] = $condition_configuration;
      }
      $conditions_collection = new ConditionPluginCollection($this->conditionsManager, $condition_values);
      $condition_values = $conditions_collection->getConfiguration();
      $form_state->setValue('conditions', $condition_values);
    }

    parent::submitForm($form, $form_state);

  }

  /**
   * Ajax callback to update the form.
   */
  public function ajaxUpdateForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Add variable information from selected mail plugin.
   *
   * @param $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param $template
   *   The current template.
   *
   * @return void
   */
  protected function showTemplateVariables(&$form, FormStateInterface $form_state, $template) {
    if ($plugin = $this->getCurrentPlugin($form_state, $template)) {
      $form['body']['widget']['#prefix'] = '<div id="body-wrapper">';
      $form['body']['widget']['#suffix'] = '</div>';
      $form['body']['default_text_container'] = [
        '#type' => 'details',
        '#title' => $this->t('Default text'),
        '#description' => $this->t('Click the button to insert the default text of the mail plugin to the body.'),
        'set_default_text' => [
          '#type' => 'button',
          '#name' => 'set_default_text',
          '#value' => $this->t('Set default text'),
          '#ajax' => [
            'callback' => '::setDefaultText',
            'event' => 'click',
            'wrapper' => 'body-wrapper',
          ],
        ],
      ];

      $variables = $plugin->getMailVariables();
      $variables = array_merge($variables, $this->moduleHandler->invokeAll('mail_plugin_' . str_replace('-', '_', $plugin->getPluginId()) . '_variables_info', [$variables]));

      if (!empty($variables)) {
        $list = '';
        foreach ($variables as $name => $description) {
          $list .= '<li><pre>{{ ' . $name . ' }}</pre> ' . $description . '</li>';
        }
        $form['body']['variables'] = [
          '#type' => 'details',
          '#title' => $this->t('Available template variables'),
          '#open' => FALSE,
        ];

        $form['body']['variables']['list'] = [
          '#type' => '#markup',
          '#markup' => '<ul>' . $list . '</ul>',
        ];
      }
    }
  }

  /**
   * Add conditions to form from selected mail plugin.
   *
   * @param $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param $template
   *   The current template.
   */
  protected function addConditions(&$form, FormStateInterface $form_state, $template) {
    if ($plugin = $this->getCurrentPlugin($form_state, $template)) {
      $conditions = $plugin->buildConditionsForm($template, $form_state);
      $form['conditions'] += $conditions;
    }
  }

  /**
   * AJAX callback to update the body field.
   */
  public function setDefaultText(array &$form, FormStateInterface $form_state) {
    $email_plugin = $this->getCurrentPlugin($form_state, $this->entity);
    $default_text = $email_plugin->getDefaultText();
    if (!empty($default_text)) {
      $form['body']['widget'][0]['value']['#value'] = $default_text;
    }
    return $form['body']['widget'];
  }

  /**
   * Returns current selected mail plugin from form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param $template
   *   The current template.
   *
   * @return \Drupal\email_content_templates\Plugin\EctMail\EctMailPluginInterface|null
   *   The mail plugin.
   */
  protected function getCurrentPlugin(FormStateInterface $form_state, EmailContentTemplateInterface $template) {
    $input = $form_state->getUserInput();
    if (!empty($input) && !empty($input['email_plugin']) && $input['email_plugin'] !== '_none') {
      /** @var \Drupal\email_content_templates\Plugin\EctMail\EctMailPluginInterface $plugin */
      $plugin = \Drupal::service('plugin.manager.ect_mail')->createInstance($input['email_plugin']);
    }
    else {
      $plugin = $template->getEmailPlugin();
    }
    return $plugin;
  }

}
