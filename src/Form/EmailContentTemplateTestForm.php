<?php

namespace Drupal\email_content_templates\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for testing emails.
 */
class EmailContentTemplateTestForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The email info manager.
   *
   * @var \Drupal\email_content_templates\EctMailPluginManager
   */
  protected $emailInfoManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->mailManager = $container->get('plugin.manager.mail');
    $instance->emailInfoManager = $container->get('plugin.manager.ect_mail');
    $instance->languageManager = $container->get('language_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ect_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $template_id = $this->getRouteMatch()->getParameter('email_content_template');
    /** @var \Drupal\email_content_templates\Entity\EmailContentTemplate $template */
    $template = $this->entityTypeManager->getStorage('email_content_template')->load($template_id);
    /** @var \Drupal\email_content_templates\Plugin\EctMail\EctMailPluginInterface $email_plugin */
    $email_plugin = $this->emailInfoManager->createInstance($template->getEmailPluginId());
    $form = array_merge_recursive($form, $email_plugin->getTestForm($form_state));

    $form['template_id'] = [
      '#type' => 'hidden',
      '#value' => $template->id(),
    ];

    $langcodes = array_keys($this->languageManager->getLanguages());

    $form['langcode'] = [
      '#type' => 'select',
      '#title' => $this->t('Language'),
      '#options' => array_combine($langcodes, $langcodes),
      '#default_value' => $form_state->getValue('langcode'),
    ];

    $form['to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recipient'),
      '#default_value' => $form_state->getValue('to', $this->currentUser()->getEmail()),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send test email'),
    ];

    $form['preview'] = [
      '#type' => 'submit',
      '#value' => $this->t('Preview email'),
      '#submit' => ['::preview'],
    ];

    return $form;
  }

  public function preview(&$form, FormStateInterface $form_state) {
    $params = $form_state->getValues();
    unset($params['op']);
    unset($params['form_id']);
    unset($params['form_build_id']);
    unset($params['form_token']);
    unset($params['preview']);
    unset($params['submit']);
    $form_state->setRedirect('entity.email_content_template.preview', ['email_content_template' => $form_state->getValue('template_id')], ['query' => $params]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $template = $this->entityTypeManager->getStorage('email_content_template')->load($form_state->getValue('template_id'));
    /** @var \Drupal\email_content_templates\Plugin\EctMail\EctMailPluginInterface $email_plugin */
    $email_plugin = $this->emailInfoManager->createInstance($template->getEmailPluginId());
    $token_map = $email_plugin->paramTokenMap();

    $params = [];
    foreach ($token_map as $param_name => $entity_type) {
      $entity_id = $form_state->getValue($param_name);
      if (!empty($entity_id)) {
        $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
        $params[$param_name] = $entity;
      }
    }

    $to = $form_state->getValue('to');
    $this->mailManager->mail($email_plugin->getMailModule(), $email_plugin->getMailKey(), $to, $form_state->getValue('langcode'), $params);
    $form_state->setRebuild();
    $this->messenger()->addStatus('Mail was sent.');
  }

}
