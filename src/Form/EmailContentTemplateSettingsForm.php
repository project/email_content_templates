<?php

namespace Drupal\email_content_templates\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for an email content template entity type.
 */
class EmailContentTemplateSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'email_content_template_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    // This form uses #config_target instead.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable email content templates'),
      '#description' => $this->t('If checked, email content templates will be enabled.'),
      '#config_target' => 'email_content_templates.settings:enabled',
    ];

    return parent::buildForm($form, $form_state);
  }

}
