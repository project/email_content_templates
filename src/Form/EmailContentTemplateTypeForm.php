<?php

namespace Drupal\email_content_templates\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\email_content_templates\EctMailPluginManager;
use Drupal\filter\Entity\FilterFormat;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for email content template type forms.
 */
class EmailContentTemplateTypeForm extends BundleEntityFormBase {

  /**
   * The ect mail plugin manager.
   *
   * @var \Drupal\email_content_templates\EctMailPluginManager
   */
  protected $emailPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.ect_mail'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(EctMailPluginManager $email_info_manager) {
    $this->emailPluginManager = $email_info_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\email_content_templates\Entity\EmailContentTemplateTypeInterface $entity_type */
    $entity_type = $this->entity;
    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add email content template type');
    }
    else {
      $form['#title'] = $this->t(
        'Edit %label template type',
        ['%label' => $entity_type->label()]
      );
    }

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $entity_type->label(),
      '#description' => $this->t('The human-readable name of this email content template type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $entity_type->getDescription(),
      '#description' => $this->t('Template type description.'),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $entity_type->status(),
      '#description' => $this->t('Check if templates of this type are enabled.'),
    ];

    $definitions = $this->emailPluginManager->getDefinitions();
    foreach ($definitions as $definition) {
      /*
       * D10.2 patch used.
       * @see https://www.drupal.org/project/drupal/issues/2269823
       */
      if (empty($definition['category'])) {
        $options[$definition['id']] = $definition['label'];
      }
      else {
        $options[$definition['id']] = $this->t('<b>[@category]</b> @label', ['@category' => $definition['category'], '@label' => $definition['label']]);
      }
    }
    asort($options);
    $form['email_plugins_wrapper'] = [
      '#type' => 'fieldset',
    ];
    $form['email_plugins_wrapper']['email_plugins'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Email plugins'),
      '#description' => $this->t('Select the allowed mail plugins.'),
      '#options' => $options,
      '#default_value' => $entity_type->getEmailPluginIds(),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::updateSelectAndButton',
        'event' => 'change',
        'wrapper' => 'select-and-button-wrapper',
      ],
    ];

    $form['default_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Subject'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getDefaultSubject(),
      '#description' => $this->t('Default subject.'),
      '#required' => FALSE,
    ];

    $form['default_body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Default Mail body text'),
      '#required' => FALSE,
      '#default_value' => $entity_type->getDefaultBody(),
      '#format' => $entity_type->getDefaultBodyFormat(),
      '#prefix' => '<div id="body-wrapper">',
      '#suffix' => '</div>'
    ];
    if (count($entity_type->getAllowedTextFormats()) !== 0) {
       $form['default_body']['#allowed_formats'] = $entity_type->getAllowedTextFormats();
    }

    $formats = filter_formats();
    $format_options = [];
    foreach ($formats as $format_id => $format) {
      $format_options[$format_id] = $format->label();
    }
    $form['allowed_text_formats'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed body text formats'),
      '#default_value' => $entity_type->getAllowedTextFormats(),
      '#options' => $format_options
    ];

    $form['default_text_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Set default body text'),
      '#attributes' => ['id' => 'select-and-button-wrapper'],
    ];
    $selected_options = $this->getPluginOptionsForSelect($form, $form_state);
    if (!empty($selected_options)) {
      $form['default_text_container']['select'] = [
        '#type' => 'select',
        '#title' => $this->t('Mail plugin'),
        '#description' => $this->t('Insert a default text of the selected mail plugin to the body, if available.'),
        '#options' => $selected_options,
      ];
      $form['default_text_container']['set_default_text'] = [
        '#type' => 'button',
        '#name' => 'set_default_text',
        '#value' => $this->t('Set default text'),
        '#ajax' => [
          'callback' => '::setDefaultText',
          'event' => 'click',
          'wrapper' => 'body-wrapper',
        ],
      ];
    }
    else {
      $form['default_text_container']['empty'] = [
        '#markup' => $this->t('Select mail plugin(s) first.'),
      ];
    }

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => [
          'Drupal\email_content_templates\Entity\EmailContentTemplateType',
          'load',
        ],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this email content template type. It must only contain lowercase letters, numbers, and underscores.'),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * AJAX callback to update the select.
   */
  public function updateSelectAndButton(array &$form, FormStateInterface $form_state) {
    return $form['default_text_container'];
  }

  /**
   * AJAX callback to update the body field.
   */
  public function setDefaultText(array &$form, FormStateInterface $form_state) {
    $selected_value = $form_state->getValue('select');
    $email_plugin = $this->emailPluginManager->createInstance($selected_value);
    $default_text = $email_plugin->getDefaultText();
    if (!empty($default_text)) {
      $form['default_body']['value']['#value'] = $default_text;
    }
    return $form['default_body'];
  }

  /**
   * Helper function to get select options.
   */
  private function getPluginOptionsForSelect($form, FormStateInterface $form_state) {
    if ($input = $form_state->getUserInput()) {
      $plugin_ids = $input['email_plugins'];
    }
    else {
      /** @var \Drupal\email_content_templates\Entity\EmailContentTemplateTypeInterface $entity_type */
      $entity_type = $this->entity;
      $plugin_ids = $entity_type->getEmailPluginIds();
    }
    $selected_options = [];
    $definitions = $this->emailPluginManager->getDefinitions();
    foreach ($plugin_ids as $key) {
      if ($key) {
        $label = $definitions[$key]['label'];
        $selected_options[$key] = $this->t('Get default text from @plugin_label', ['@plugin_label' => $label]);
      }
    }
    return $selected_options;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save template type');
    $actions['delete']['#value'] = $this->t('Delete template type');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\email_content_templates\Entity\EmailContentTemplateTypeInterface $entity_type */
    $entity_type = $this->entity;

    $entity_type->setDefaultBody(trim($form_state->getValue('default_body')['value'] ?? ''));
    $entity_type->setDefaultBodyFormat(trim($form_state->getValue('default_body')['format'] ?? ''));
    $allowed_plugins = array_filter($form_state->getValue('email_plugins'));
    sort($allowed_plugins);
    $entity_type->setEmailPlugins($allowed_plugins);
    $allowed_text_formats = array_keys(array_filter($form_state->getValue('allowed_text_formats')));
    $entity_type->setAllowedTextFormats($allowed_text_formats);
    $status = $entity_type->save();
    $t_args = ['%name' => $entity_type->label()];
    if ($status == SAVED_UPDATED) {
      $message = $this->t('The template type %name has been updated.', $t_args);
    }
    elseif ($status == SAVED_NEW) {
      $message = $this->t('The template type %name has been added.', $t_args);
    }
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($entity_type->toUrl('collection'));
    return $status;
  }

}
