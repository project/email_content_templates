<?php

namespace Drupal\email_content_templates\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\email_content_templates\Entity\EmailContentTemplateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class PreviewController extends ControllerBase {

  /**
   * The ect mail plugin manager.
   *
   * @var \Drupal\email_content_templates\EctMailPluginManager
   */
  protected $emailPluginManager;

  /**
   * The ect service.
   *
   * @var \Drupal\email_content_templates\EctServiceInterface
   */
  protected $ectService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();

    $instance->emailPluginManager = $container->get('plugin.manager.ect_mail');
    $instance->ectService = $container->get('email_content_templates.service');
    return $instance;
  }

  public function preview(EmailContentTemplateInterface $email_content_template, Request $request) {
    /** @var \Drupal\email_content_templates\Entity\EmailContentTemplateInterface $template */
    $template = $this->entityTypeManager()->getStorage('email_content_template')->load($request->get('template_id'));
    /** @var \Drupal\email_content_templates\Plugin\EctMail\EctMailPluginInterface $email_plugin */
    $email_plugin = $this->emailPluginManager->createInstance($template->getEmailPluginId());
    $token_map = $email_plugin->paramTokenMap();
    $params = ['template' => $template];
    foreach ($token_map as $param_name => $entity_type) {
      $entity_id = $request->get($param_name);
      if (!empty($entity_id)) {
        $entity = $this->entityTypeManager()->getStorage($entity_type)->load($entity_id);
        $params[$param_name] = $entity;
      }
    }

    $plugin_id = explode('-', $email_plugin->getPluginId(), 2);
    $message = [
      'module' => $plugin_id[0],
      'key' => $plugin_id[1],
      'id' => $email_plugin->getPluginId(),
      'params' => $params,
    ] + $request->query->all();
    $this->ectService->prepareMessage($message, $template);
    return $message['body'];
  }

}
