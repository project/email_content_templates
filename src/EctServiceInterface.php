<?php

namespace Drupal\email_content_templates;

use Drupal\email_content_templates\Entity\EmailContentTemplateInterface;
use Drupal\email_content_templates\Plugin\EctMail\EctMailPluginInterface;

/**
 * Provides Email Preparation functionality.
 */
interface EctServiceInterface {

  /**
   * Returns a list of matching templates for the given mail.
   *
   * @param array $mail
   *   The email array as from hook_mail.
   *
   * @return \Drupal\email_content_templates\Entity\EmailContentTemplateInterface[]
   *   The email content templates.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function findTemplatesForMail(array $mail);

  /**
   * Helper function for preparing mails with template values.
   *
   * @param array $mail
   *   The message array.
   * @param \Drupal\email_content_templates\Entity\EmailContentTemplateInterface $template
   *   The template.
   * @param \Drupal\email_content_templates\Plugin\EctMail\EctMailPluginInterface|null $plugin
   *   The plugin.
   */
  public function prepareMessage(array &$mail, EmailContentTemplateInterface $template, EctMailPluginInterface $plugin = NULL);

}
